# Supplementary material (code and dataset) for "Collaborative Ad Transparency: Promises and Limitations"

_**License**: This code is licensed under the MIT license. If you in any way use this code for research that results in publications, please cite our paper as described below:_

> E. Gkiouzepi, A. Andreou, O. Goga, P. Loiseau, "Collaborative Ad Transparency: Promises and Limitations" in _IEEE S&P_, 2023, doi: 10.1109/SP46215.2023.00079.

## _Generation of a Synthetic Population_

### Required files

| Description | Filename |
| ------ | ------ |
| R code | `create-simulated_users420_1M_322attr.R` |
| Dataset $`\mathcal{D}_p`$ | `facebook_estimations_2019-06-09.csv` |
| Dataset $`\mathcal{D}_{\texttt{attributes}}`$ | `ntis_420_322attr.csv` |

At lines 15-17 of R code file, are showcased two examples of input data, estimations of reach or number of occurences.

All files referenced in this section can be found under the `generation_of_a_synthetic_population` folder.

#### Dataset $`\mathcal{D}_p`$ 

If you choose to use the Dataset $`\mathcal{D}_p`$, you should uncomment line 15 and comment lines 16-17 of R code.
```r
fbest=read.csv("facebook_estimations_2019-06-09.csv",header=F, sep = ",", dec = ".")
#fbcount=read.csv("ntis_420_322attr.csv",header=F, sep = " ")
#fbest=fbcount/420
```

and in line 53 set the number of attributes to 322

```r
maxdim = 322
```

The `facebook_estimations_2019-06-09.csv` file is a square matrix of 322 rows and 322 columns containing numbers between 0 and 1. Thi matrix approximates the pairwise probabilities of each attribute  in its non-diagonal positions and in the diagonal approximates the marginal probabilites. 

This data was collected as described in section III-A of [[1](#references)].
>We collect the number of Facebook users satisfying a particular formula ( $`N_{\theta}`$ ) from the Facebook Ads Manager. [...] The system (Facebook Ads Manager) was queried with every possible combination of one or two attributes on 09/06/2019 and gathered its worldwide monthly active users estimates. We collected this data for 322 curated interest attributes, hence in total, we collected 51,681 $`N_{\theta}`$ values.

$`N_{\theta}`$ values are divided by the total number of Facebook users to produce a matrix to approximate the pairwise and marginal probabilities.

#### Dataset $`\mathcal{D}_{\text{attributes}}`$

If you choose to use the dataset $`\mathcal{D}_{\texttt{attributes}}`$ with 420 user profiles, you should comment line 15 and uncomment lines 16-17 of R code.
```r
#fbest=read.csv("facebook_estimations_2019-06-09.csv",header=F, sep = ",", dec = ".")
fbcount=read.csv("ntis_420_322attr.csv",header=F, sep = " ")
fbest=fbcount/420
```

and in line 53 set the number of attributes to 322

```r
maxdim = 322
```

The `ntis_420_322attr.csv` file contains the Dataset $`\mathcal{D}_{\texttt{attributes}}`$, i.e. a matrix of the number of occurences for every possible combination of one or two attributes out of 322 attributes in the sample of 420 users. In the diagonal is the occurences of single attributes. This matrix of 322 rows and 322  columns containing integers between 0 and 420 is then divided by the total number of users in the sample, i.e. 420, to produce a matrix of probabilities between 0 and 1.

#### R code

The file `create-simulated_users420_1M_322attr.R`  implements the Algorithm 1 described in section IV of [[1](#references)] to generate a representative synthetic population. Packages “bindata” [[3](#references)] and "Matrix" [[2](#references)] are required.

The line 53 can be adjusted accordingly to accomodate different number of attributes(`maxdim`) as input , and the line 54 can be changed to the desired number of simulated users(`n`) to produce as output.

```r
maxdim = 322
n=1000000
```

### Output

| Variable | Filename |
| ------ | ------ |
| bindata_test | `users_1e+06_322attr_bindata.RData` |

This file contain one variable each. This variable stores a binary matrix (each row of the matrix represents a user, each column represents an attribute).
This matrix contains only 0's or 1's:
- 0 in *n*th row and *m*th column means that *n*th user doesn't satisfy the *m*th attribute,
- 1 in *n*th row and *m*th column means that *n*th user satisfies the *m*th attribute.

## _Simulation over 1000 ads_

### Required files

| Description | Filename |
| ------ | ------ |
| R-Markdown script | `20221204_Simulated_experiments.Rmd` |
| Synthetic population of 1 million users | `users_1M_322attr.RData` |
| $`N_{\theta}`$ of synthetic population | `Nt_bindata.RData` |
| Attributes names | `attr_names.RData` |

All files referenced in this section can be found under the `simulation_over_1000_ads` folder.

#### R-Markdown script

The file `20221204_Simulated_experiments.Rmd`  implements the Algorithm 2 described in section V-C of [[1](#references)] to simulate an Ad Campaign over a synthetic population and infer the targeting formula.

**Input parameters**
| Symbol | Variable | Description|
| ------ | ------ | ------ |
| $`K`$ | `p` | Ad Campaign parameter (double (0,1])|
| $`\theta`$ | `ad` | Targeted formula (integer vector)|
| $`N_m`$ | `Nm` | Number of monitored users (positive integer)||
| $`\hat{K}`$ | `phat` | Inference parameter (double (0,1])|
| $`n_{runs}`$ | `nexp` | Simulation parameter (positive integer)|
| $`N_{\theta}`$ | `Nt` | Pairwise/marginal attributes occurences (matrix of integers) |
| $`\mathcal{N}`$ | `users` | Population set (binary matrix) |
| $`\mathcal{A}`$ | `attr_names` | Attributes set (characters vector) |

### Output

| Variable | Filename |
| ------ | ------ |
| Output folder | `./output` | 

The output folder contains a (tree) structure of subfolders, organized (branches) by values of  $`\hat{K}`$, $`K`$ and $`N_m`$. At the end nodes (leaves) of this (tree) structure, are csv files containing the following results.

| Column | Description |
| ------ | ------ |
| Experiment | integer denoting the iteration |
| Simulated_users | Number of total population |
| Audience_targeted_Nt | $`N_{\theta}`$ of targeted formula |
| Attributes_targeted | Targeted formula $`\theta`$ |
| Users_receiving_ad | Number of total users receiving ad, $`K \cdot N_{\theta}`$ |
| Monitored_audience | Number of Monitored users satisfying $`\theta`$|
| Monitored_users_receiving_ad | Number of Monitored users receiving ad |
| Common_attributes | $`A_r`$, number of shared attributes among monitored users |
| Possible_patterns |  $`T_r`$, number of possible targeted formulas|
| Accurate_prediction_k | 1, if infered correctly both attributes, 0 otherwise|
| Only_one_attribute_predicted_k |1, if infered correctly only one attributes, 0 otherwise|
| None_predicted_k | 1, if no correct attribute inferred, 0 otherwise|
| Attributes_predicted_k | Formula inferred in this form: (Name of attr#1) AND (Name of attr attr#2) |
| Found_within_5_most_likely| Rank of correct targeted formula based on likelihood inferred is less than 6| 
| Rank_of_targeted | Rank of correct targeted formula based on likelihood inferred| 
| Ties_first_place | Number of formulas ties in rank#1 |

## _AdAnalyst Consent Forms_

The AdAnalyst consent forms discussed in Appendix D of our paper [[1](#references)] are included in `consent_2017_2019.pdf`.

## References

1. E. Gkiouzepi, A. Andreou, O. Goga, P. Loiseau, "Collaborative Ad Transparency: Promises and Limitations" in _IEEE S&P_, 2023, [doi:10.1109/SP46215.2023.00079](https://doi.ieeecomputersociety.org/10.1109/SP46215.2023.00079)
2. D. Bates and M. Maechler, *Matrix: Sparse and Dense Matrix Classes and Methods*, 2019, R package version 1.2-18. [Online]. Available: https://cran.r-project.org/package=Matrix
3.  F. Leisch and A. Weingessel, "The bindata package," 2006
